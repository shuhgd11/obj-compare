package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.comparator.AttributesComparator;
import org.smartboot.compare.comparator.Comparator;
import org.smartboot.compare.comparator.StringComparator;
import org.smartboot.compare.comparator.TagComparator;
import org.smartboot.compare.difference.BaseDifference;
import org.smartboot.compare.difference.Difference;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2021/2/3 11:43:28
 */
public class CompareTest {

    @Test
    public void testCompareSimple() {
        User expect = new User();
        expect.setId("1");
        expect.setPassword("111212");
        expect.setUsername("Tom");
        expect.setSex("male");

        User target = new User();
        target.setId("2");
        target.setPassword("1112112");
        target.setUsername("Jerry");
        target.setSex("male");

        CompareResult result = CompareHelper.compare(expect, target);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
    }

    @Test
    public void testCompareCollection() {
        List<Integer> userIds = new ArrayList<>(8);
        userIds.add(1);
        userIds.add(2);
        userIds.add(3);

        List<Integer> comparedIds = new ArrayList<>(8);
        comparedIds.add(1);
        comparedIds.add(2);
        comparedIds.add(4);

        CompareResult result = CompareHelper.compare(userIds, comparedIds);
        System.out.println(result.toString());
        Assert.assertFalse(result.isSame());
    }


    @Test
    public void testCompareObj() {
        User expect = new User();
        expect.setId("1");
        expect.setPassword("111212");
        expect.setUsername("278821");
        expect.setSex("male");

        //////////////////////////////////////////////

        User target = new User();
        target.setId("2");
        target.setPassword("1112112");
        target.setUsername("278821");
        target.setSex("male");

        List<IgnoreField> ignoreFields = new ArrayList<>();
        // 忽略类型为String.class 字段名为id的 属性比较
        ignoreFields.add(new IgnoreField("id", String.class));

        // 忽略任何以word结尾的属性，类型为任意类型
        IgnorePatternField ignorePatternField = new IgnorePatternField("[\\s\\d]*word");
        ignoreFields.add(ignorePatternField);

        CompareResult result = CompareHelper.compare(expect, target, ignoreFields);
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testRecycle() {
        RecycleUser user = new RecycleUser();
        RecycleUser _user = new RecycleUser();

        user.setName("qinluo");
        user.setPassword("haolo2");
        user.setUser(_user);


        _user.setName("qinluo");
        _user.setPassword("haolo1");
        _user.setUser(user);

        CompareResult result = CompareHelper.compare(user, _user);
        System.out.println(result);
    }

    @Test
    public void testRecycle02() {
        List<Object> expect = new ArrayList<>();
        List<Object> actual = new ArrayList<>();

        expect.add(1);
        expect.add(3);
        expect.add(actual);

        actual.add(1);
        actual.add(2);
        actual.add(expect);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
    }

    @Test
    public void testCompareTag() {
        // TagNamedTypeCompare和AttributesNamedTypeCompare已经内置，默认使用,分隔tag, 使用;分隔attribute，如果有需求，可以进行覆盖
        User user = new User();
        user.setTag("123,456,789,987");
        user.setAttribute("fee:100;timestamp:1234567;is_service:1;");

        User user2 = new User();
        user2.setTag("123,456,789,987,321");
        user2.setAttribute("fee:100;timestamp:123456;is_service:2;style:B");

        CompareResult result = CompareHelper.compare(user, user2);
        System.out.println(result);

        user.setTag("");
        user.setAttribute(null);
        user2.setTag(null);
        user2.setAttribute(" ");

        result = CompareHelper.compare(user, user2, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
    }

    @Test
    public void testLooseMode() {
        List<String> names = new ArrayList<>(100);
        names.add("tom");
        names.add("Jerry");
        names.add(null);

        List<String> linkedNames = new LinkedList<>();
        linkedNames.add("tom");
        linkedNames.add("Jerry");
        linkedNames.add("");

        // default not strict mode
        CompareResult result = CompareHelper.compare(names, linkedNames, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);

        System.out.println("===================================\n");

        result = CompareHelper.compare(names, linkedNames);
        System.out.println(result);
    }

    @Test
    public void testRecycle03() {
        List<Object> expect = new ArrayList<>();
        List<Object> actual = new ArrayList<>();

        expect.add(1);
        expect.add(3);
        expect.add("a");

        actual.add(1);
        actual.add(2);
        actual.add(1);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        System.out.println(Object.class.isAssignableFrom(Object.class));
    }

    @Test
    public void testCustomizedComparator() {
        RecycleUser user = new RecycleUser();
        RecycleUser _user = new RecycleUser();

        user.setName("qinluo");
        user.setPassword("haolo2");
        user.setUser(_user);


        _user.setName("qinluo");
        _user.setPassword("haolo1");
        _user.setUser(user);

        // always equals.
        ComparatorRegister.register(RecycleUser.class, (Comparator<RecycleUser>) context -> Difference.SAME);

        CompareResult result = CompareHelper.compare(user, _user);
        System.out.println(result);

        Assert.assertTrue(result.isSame());

        ComparatorRegister.unregister(RecycleUser.class);
    }

    @Test
    public void testCustomizedEquals() {
        EqualsUser expect = new EqualsUser();
        expect.name = "Tom";
        expect.sex = "man";
        expect.idCard = "123456";

        EqualsUser actual = new EqualsUser();
        actual.name = "Jerry";
        actual.sex = "woman";
        actual.idCard = "123456";

        // Use equals
        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertTrue(result.isSame());

        // Disable equals method.
        result = CompareHelper.compare(expect, actual, Option.mix(Option.DISABLE_EQUALS));
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);
    }

    private class EqualsUser {
        private String name;
        private String sex;
        private String idCard;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            EqualsUser that = (EqualsUser) o;
            // idCard相等就相等
            return Objects.equals(idCard, that.idCard);
        }
    }


    @Test
    public void testGenericType() {
        Assert.assertEquals(Integer.class, new DirectSubclass().getType());
        Assert.assertEquals(String.class, new StringSubclass().getType());
        Assert.assertEquals(String.class, new DirectExtendsSubclassImpl().getType());
        Assert.assertEquals(Boolean.class, new DirectChangeGenericPosSubclassImpl().getType());
        Assert.assertEquals(Character.class, new DirectChangeGenericPosSubclassImpl2<>().getType());
        Assert.assertEquals(Character.class, new DirectChangeGenericPosSubclassImpl3().getType());
        Assert.assertEquals(Long.class, new ChangeGenericNameSubclassImpl().getType());
        Assert.assertEquals(Date.class, new InterruptGenericSubclassImpl().getType());
        Assert.assertEquals(Double.class, new DirectMultipleGenericSubclassImpl().getType());
        Assert.assertEquals(Comparator.class, new RechangeGenericPosImpl().getType());
        Assert.assertEquals(Float.class, new ChangeGenericPosSubclass02SubclassImpl().getType());
        Assert.assertEquals(List.class, new ParameterizedTypeSubclass().getType());
        Assert.assertEquals(Object.class, new UnrealizedGenericTypeSkipCheck2<Integer>().getType());
        Assert.assertEquals(Object.class, new UnrealizedGenericTypeSkipCheck2<Exception>().getType());

        Assert.assertEquals(User[].class, new GenericArrayComparatorImpl().getType());
        Assert.assertEquals(User[][].class, new GenericArrayComparatorSubclassImpl().getType());
        Assert.assertEquals(User[][][].class, new GenericArrayComparatorSubclassImpl2().getType());
        Assert.assertEquals(User[][][][][].class, new GenericArrayComparatorSubclassChangePosImpl().getType());
    }

    @Test(expected = IllegalStateException.class)
    public void testUnrealized() {
        Assert.assertEquals(Object.class, new UnrealizedGenericType<Integer>().getType());
    }

    @Test
    public void testCustomizedComparatorWithSkipCheck() {
        ComparatorRegister.register(NameType.of(Integer.class, "a"), new UnrealizedGenericTypeSkipCheck<Integer>() {

            @Override
            public Difference compare(Integer expect, Integer actual, ComparatorContext<Integer> context) {
                if (Objects.equals(expect, actual)) {
                    return Difference.SAME;
                }
                return new SourceDifference(this, context.getPath(), expect, actual);
            }
        });

        ComparatorRegister.register(NameType.of(String.class, "b"), new UnrealizedGenericTypeSkipCheck<String>() {

            @Override
            public Difference compare(String expect, String actual, ComparatorContext<String> context) {
                if (Objects.equals(expect, actual)) {
                    return Difference.SAME;
                }
                return new SourceDifference(this, context.getPath(), expect, actual);
            }
        });

        Map<String, Object> expect = new HashMap<>();
        expect.put("a", 4);
        expect.put("b", "Hello Word");

        Map<String, Object> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", "Hello Word !!!");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());

        List<SourceDifference> differences = result.getDifferences(SourceDifference.class);
        Assert.assertEquals(differences.size(), 2);

        for (SourceDifference sd : differences) {
            System.out.println(sd.getComparator().getClass().getName());
            System.out.println(((AbstractComparator)sd.getComparator()).getType());
//            Assert.assertEquals(((AbstractComparator)sd.getComparator()).getType(), Object.class);
        }

        ComparatorRegister.unregister(NameType.of(String.class, "b"));
        ComparatorRegister.unregister(NameType.of(Integer.class, "a"));
    }

    @Test
    public void testBuiltinComparator() {
        List<Object> keys = ComparatorRegister.getAllRegisterKeys();
        for (Object key : keys) {
            Comparator<?> comparator = null;
            if (key instanceof Class) {
                comparator = ComparatorRegister.findComparator((Class)key);
            } else if (key instanceof NameType) {
                comparator = ComparatorRegister.findComparator((NameType)key);
            }

            if (comparator == null) {
                System.out.println("cannot find comparator with key " + key);
            } else if (comparator instanceof AbstractComparator){
                System.out.println(key + ", AbstractComparator "
                        + comparator.getClass().getSimpleName() + " type : "
                        + ((AbstractComparator<?>) comparator).getType());
            } else {
                System.out.println(key + ", Comparator " + comparator.getClass().getSimpleName());
            }

        }
    }

    @Test
    public void testCompare() {
        ComparatorRegister.register(NameType.of(Comparator.class, "a"), new RechangeGenericPosImpl());
        ComparatorRegister.register(NameType.of(int[].class, "a"), new AbstractComparator<int[]>() {

            @Override
            public Difference compare(int[] expect, int[] actual, ComparatorContext<int[]> context) {
                if (Arrays.equals(expect, actual)) {
                    return (new BaseDifference(context.getPath(), expect, actual));
                }
                return null;
            }
        });

        ComparatorRegister.unregister(NameType.of(Comparator.class, "a"));
        ComparatorRegister.unregister(NameType.of(int[].class, "a"));
    }

    static class DirectSubclass extends AbstractComparator<Integer> {
        @Override
        public Difference compare(Integer expect, Integer actual, ComparatorContext<Integer> context) {
            return null;
        }
    }

    static class StringSubclass extends StringComparator { }

    static abstract class DirectExtendsSubclass<T> extends AbstractComparator<T> { }

    static class DirectExtendsSubclassImpl extends DirectExtendsSubclass<String> {
        @Override
        public Difference compare(String expect, String actual, ComparatorContext<String> context) {
            return null;
        }
    }

    // Move T(index 0) to T(index 1)
    static abstract class ChangeGenericPosSubclass<R, T> extends AbstractComparator<T> { }

    static class DirectChangeGenericPosSubclassImpl extends ChangeGenericPosSubclass<Integer, Boolean> {
        @Override
        public Difference compare(Boolean expect, Boolean actual, ComparatorContext<Boolean> context) {
            return null;
        }
    }

    static class DirectChangeGenericPosSubclassImpl2<T> extends ChangeGenericPosSubclass<T, Character> {
        @Override
        public Difference compare(Character expect, Character actual, ComparatorContext<Character> context) {
            return null;
        }
    }

    static class DirectChangeGenericPosSubclassImpl3 extends DirectChangeGenericPosSubclassImpl2<Integer> { }

    static abstract class DirectMultipleGenericSubclass<R, V> extends AbstractComparator<R> { }

    static class DirectMultipleGenericSubclassImpl extends DirectMultipleGenericSubclass<Double, Integer> {
        @Override
        public Difference compare(Double expect, Double actual, ComparatorContext<Double> context) {
            return null;
        }
    }

    static abstract class RechangeGenericPos<T, R> extends ChangeGenericPosSubclass<T, R> { }

    static class RechangeGenericPosImpl extends RechangeGenericPos<Integer, Comparator> {
        @Override
        public Difference compare(Comparator expect, Comparator actual, ComparatorContext<Comparator> context) {
            return null;
        }
    }

    static abstract class InterruptGeneric<T, R> extends AbstractComparator<T> { }

    static abstract class InterruptGenericSubclass<T> extends InterruptGeneric<Date, T> { }

    static class InterruptGenericSubclassImpl extends InterruptGenericSubclass<Integer> {
        @Override
        public Difference compare(Date expect, Date actual, ComparatorContext<Date> context) {
            return null;
        }
    }


    static abstract class ChangeGenericPosSubclass02<R, T> extends AbstractComparator<T> { }

    static abstract class ChangeGenericPosSubclass02Subclass<R> extends ChangeGenericPosSubclass02<R, Float> { }

    static class ChangeGenericPosSubclass02SubclassImpl extends ChangeGenericPosSubclass02Subclass<Integer> {
        @Override
        public Difference compare(Float expect, Float actual, ComparatorContext<Float> context) {
            return null;
        }
    }

    static abstract class ChangeGenericName<T, R> extends AbstractComparator<T> { }

    static abstract class ChangeGenericNameSubclass<K, R> extends ChangeGenericName<R, Integer> { }

    static class ChangeGenericNameSubclassImpl extends ChangeGenericNameSubclass<String, Long> {
        @Override
        public Difference compare(Long expect, Long actual, ComparatorContext<Long> context) {
            return null;
        }
    }

    static class UnrealizedGenericType<T> extends AbstractComparator<T> {
        @Override
        public Difference compare(T expect, T actual, ComparatorContext<T> context) {
            return null;
        }
    }

    static class ParameterizedTypeSubclass extends UnrealizedGenericType<List<?>> {

    }

    static class UnrealizedGenericTypeSkipCheck<T> extends AbstractComparator<T> {

        public UnrealizedGenericTypeSkipCheck() {
//            super(null);
        }

        @Override
        public Difference compare(T expect, T actual, ComparatorContext<T> context) {
            return null;
        }
    }

    static class UnrealizedGenericTypeSkipCheck2<T> extends AbstractComparator<T> {

        public UnrealizedGenericTypeSkipCheck2() {
            super(null);
        }

        @Override
        public Difference compare(T expect, T actual, ComparatorContext<T> context) {
            return null;
        }
    }

    static abstract class GenericArrayComparator<T> extends AbstractComparator<T[]> {}

    static class GenericArrayComparatorImpl extends GenericArrayComparator<User> {
        @Override
        public Difference compare(User[] expect, User[] actual, ComparatorContext<User[]> context) {
            return null;
        }
    }

    static abstract class GenericArrayComparatorSubclass<T> extends GenericArrayComparator<T[]> {}

    static abstract class GenericArrayComparatorSubclassChangePos<K, T> extends GenericArrayComparator<T[]> {}

    static class GenericArrayComparatorSubclassImpl extends GenericArrayComparatorSubclass<User> {
        @Override
        public Difference compare(User[][] expect, User[][] actual, ComparatorContext<User[][]> context) {
            return null;
        }
    }

    static class GenericArrayComparatorSubclassImpl2 extends GenericArrayComparatorSubclass<User[]> {
        @Override
        public Difference compare(User[][][] expect, User[][][] actual, ComparatorContext<User[][][]> context) {
            return null;
        }
    }

    static class GenericArrayComparatorSubclassChangePosImpl extends GenericArrayComparatorSubclassChangePos<Long, User[][][]> {
        @Override
        public Difference compare(User[][][][][] expect, User[][][][][] actual, ComparatorContext<User[][][][][]> context) {
            return null;
        }
    }
}
