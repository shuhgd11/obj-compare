package org.smartboot.compare;

import java.util.Objects;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 15:03
 */
public class IgnoreField {

    /**
     * The ignored field name.
     */
    protected String name;

    /**
     * If field not full-match field name, but it's ends with field.
     */
    protected boolean applyPath;

    /**
     * Which type is ignored.
     *
     * If ignoreType is Object.class, it's means any type that fields matched is ignored.
     */
    protected Class<?> ignoreType;

    /**
     * If applyAssignable is true, will ignore all type that is sub type of ignoreType.
     */
    protected boolean applyAssignable = true;

    protected TypeAssigner assigner;

    public IgnoreField(String fieldName) {
        this.name = fieldName;
        this.applyPath = true;
        this.ignoreType = Object.class;
    }

    public IgnoreField(String fieldName, Class<?> ignoreType) {
        this.name = fieldName;
        this.applyPath = true;
        this.ignoreType = ignoreType;
    }

    public void setApplyAssignable(boolean applyAssignable) {
        this.applyAssignable = applyAssignable;
    }

    public void setIgnoreType(Class<?> ignoreType) {
        this.ignoreType = ignoreType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setApplyPath(boolean applyPath) {
        this.applyPath = applyPath;
    }

    public boolean ignored(ComparatorContext<?> context, NameType nameType, Path newPath) {
        if (Objects.equals(name, nameType.getName()) && checkType(nameType.getType())) {
            return true;
        }

        if (!applyPath) {
            return false;
        }

        return Objects.equals(name, newPath.getFullPath()) && checkType(nameType.getType());

    }

    protected boolean checkType(Class<?> type) {
        init();
        return assigner.checkCast(type);
    }

    private void init() {
        if (assigner == null) {
            assigner = new TypeAssigner(applyAssignable, ignoreType);
        }
    }
}
