package org.smartboot.compare;

import java.util.List;

/**
 * @author qinluo
 * @date 2022-03-11 18:28:43
 * @since 1.0.0
 */
public class IgnoreFieldFilter implements FieldFilter {

    /**
     * The ignored filed config.
     */
    private List<IgnoreField> ignoreFields;

    public void updateIgnoreFields(List<IgnoreField> ignoreFields) {
        this.ignoreFields = ignoreFields;
    }

    @Override
    public boolean filtered(FieldCache f, ComparatorContext<?> context, Path curPath) {
        NameType nameType = f.getNamedType();
        List<IgnoreField> ifs = ignoreFields;

        if (ifs == null || ifs.isEmpty()) {
            return false;
        }

        for (IgnoreField ignoreField : ifs) {
            if (ignoreField.ignored(context, nameType, curPath)) {
                return true;
            }
        }

        return false;
    }
}
