package org.smartboot.compare;

/**
 * @author qinluo
 * @date 2022-03-11 19:24:57
 * @since 1.0.0
 */
public class Item {

    private Long id;
    @Snapshoted
    private String name;
    @Snapshoted
    private String url;

    private int stock;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
