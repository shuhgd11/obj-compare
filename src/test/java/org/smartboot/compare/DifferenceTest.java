package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;
import org.smartboot.compare.difference.DifferenceError;

/**
 * @author qinluo
 * @date 2024-04-04 12:17:32
 * @since 1.0.8
 */
public class DifferenceTest {

    class U1 {

        @Override
        public boolean equals(Object obj) {
            throw new RuntimeException("U1");
        }
    }

    @Test
    public void testError() {
        U1 u1 = new U1();
        U1 u2 = new U1();

        CompareResult result = CompareHelper.compare(u1, u2);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences(DifferenceError.class).size(), 1);
    }
}
