package org.smartboot.compare.difference;

import org.smartboot.compare.Path;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 15:44
 */
public class AttributeCompareDifference extends AbstractDifference {

    private final List<BaseDifference> notEqualsFailItems = new ArrayList<>();

    private final List<String> expectKeys = new ArrayList<>();

    private final List<String> compareKeys = new ArrayList<>();

    public List<String> getExpectKeys() {
        return expectKeys;
    }

    public List<String> getCompareKeys() {
        return compareKeys;
    }

    public AttributeCompareDifference(Path path) {
        super(path);
    }

    public List<BaseDifference> getNotEqualsFailItems() {
        return notEqualsFailItems;
    }

    @Override
    public String getMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append(type()).append(", ").append(path).append("\n\t");

        if (!notEqualsFailItems.isEmpty()) {
            sb.append("\t");
            sb.append("** 不相等的属性项 *** ");
            for (BaseDifference failItem : notEqualsFailItems) {
                sb.append("\n\t\t\t");
                sb.append(failItem);
            }
        }

        boolean hasDifferenceFailItem = (compareKeys.size() != 0 || expectKeys.size() != 0);
        if (hasDifferenceFailItem) {
            sb.append("\n\t\t");
            sb.append("** 属性项补集 *** ");
            sb.append("集合Key包含不一致, 期望对象补集为 ").append(expectKeys).append(", 实际对象补集为 ").append(compareKeys);
        }

        return sb.toString();
    }

    @Override
    public String type() {
        return "CustomizedAttribute";
    }
}
