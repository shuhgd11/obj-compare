
package org.smartboot.compare.array;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.constants.CompareConstants;
import org.smartboot.compare.difference.Difference;

/**
 * @author qinluo
 * @date 2024-03-24 01:40:51
 * @since 1.0.7
 */
public class CharacterArrayComparator extends AbstractComparator<Character[]> {

    @Override
    public Difference compare(Character[] expect, Character[] actual, ComparatorContext<Character[]> context) {
        return ArrayHelper.compare(expect, actual, context, CompareConstants.WRAPPER);
    }
}
