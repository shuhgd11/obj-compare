package org.smartboot.compare.version;

import com.alibaba.fastjson2.JSON;
import org.junit.Assert;
import org.junit.Test;
import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.CompareHelper;
import org.smartboot.compare.CompareResult;
import org.smartboot.compare.Configuration;
import org.smartboot.compare.FeatureFunction;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * Version 1.1.1
 *
 * @author qinluo
 * @date 2024-07-16 19:42:08
 * @since 1.0.0
 */
public class Version111Test {

    @Test
    public void testConvertStringAsJson() {

        String expectJson = "{\"username\": \"Tom\", \"type\":\"Cat\"}";
        String actualJson = "{\"username\": \"Jerry\", \"type\":\"Mouse\"}";

        ComparatorContext<Object> ctx = ComparatorContext.of(expectJson, actualJson);
        ctx.setConfiguration(new Version111Configuration(true));

        AtomicBoolean converted = new AtomicBoolean();

        ctx.setFeatureFunction(new FeatureFunction() {
            @Override
            public Object convertString2Object(ComparatorContext<?> ctx, String value) {
                Version111Configuration cfg = ctx.getConfiguration().asSubConfiguration();

                if (ctx.getPath().isRoot()
                        && cfg.getOnlyConvertJsonAtRoot()) {
                    ctx.addMessage("convert", "convert string to json in root");
                    converted.set(true);
                    return JSON.parseObject(value);
                }

                else if (cfg.getOnlyConvertJsonAtRoot()) {
                    ctx.addMessage("convert", "reject convert string to json in " + ctx.getPath().getFullPath());
                    return null;
                }

                converted.set(true);
                ctx.addMessage("convert", "convert string to json in " + ctx.getPath().getFullPath());
                return JSON.parseObject(value);
            }
        });

        CompareResult result = CompareHelper.compare(ctx);
        System.out.println(result);

        Assert.assertTrue(converted.get());
        Assert.assertEquals(result.getDifferences().size(), 2);
    }

    static class Version111Configuration implements Configuration {
        private boolean onlyConvertJsonAtRoot;

        public Version111Configuration(boolean onlyConvertJsonAtRoot) {
            this.onlyConvertJsonAtRoot = onlyConvertJsonAtRoot;
        }

        public boolean getOnlyConvertJsonAtRoot() {
            return onlyConvertJsonAtRoot;
        }
    }
}
