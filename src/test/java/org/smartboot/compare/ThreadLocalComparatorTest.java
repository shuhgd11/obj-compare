package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;
import org.smartboot.compare.comparator.StringComparator;
import org.smartboot.compare.difference.Difference;

import java.util.concurrent.atomic.AtomicBoolean;

/**
 * @author qinluo
 * @date 2024-04-05 21:44:39
 * @since 1.0.0
 */
public class ThreadLocalComparatorTest {

    @Test
    public void testThreadLocal() {

        ThreadLocalComparatorRegister.register(String.class, new StringComparator() {
            @Override
            public Difference compare(String expect, String actual, ComparatorContext<String> context) {
                // Any strings are equals.
                return null;
            }
        });

        String expect = "Tom";
        String actual = "Jerry";

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertTrue(result.isSame());


        // Secondary check.
        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertTrue(result.isSame());

        // Remove registered comparators.
        ThreadLocalComparatorRegister.removeAll();

        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(1L, result.getDifferences().size());
    }

    @Test
    public void testThreadLocalNameType() {

        ThreadLocalComparatorRegister.register(NameType.of(String.class, "username"), new StringComparator() {
            @Override
            public Difference compare(String expect, String actual, ComparatorContext<String> context) {
                // Any strings are equals.
                return null;
            }
        });

        User expect = new User();
        expect.setUsername("Tom");
        expect.setPassword("aaa");

        User actual = new User();
        actual.setUsername("Jerry");
        actual.setPassword("bbb");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(1L, result.getDifferences().size());


        // Secondary check.
        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(1L, result.getDifferences().size());

        // Remove registered comparators.
        ThreadLocalComparatorRegister.removeAll();

        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(2L, result.getDifferences().size());
    }

    @Test
    public void testThreadLocalInOtherThread() {
        User expect = new User();
        expect.setUsername("Tom");
        expect.setPassword("aaa");

        User actual = new User();
        actual.setUsername("Jerry");
        actual.setPassword("bbb");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(2L, result.getDifferences().size());

        AtomicBoolean executed = new AtomicBoolean();

        new Thread(() -> {
            ThreadLocalComparatorRegister.register(NameType.of(String.class, "username"), new StringComparator() {
                @Override
                public Difference compare(String expect1, String actual1, ComparatorContext<String> context) {
                    // Any strings are equals.
                    return null;
                }
            });

            CompareResult result1 = CompareHelper.compare(expect, actual);
            System.out.println(result1);

            Assert.assertFalse(result1.isSame());
            Assert.assertEquals(1L, result1.getDifferences().size());

            executed.set(true);
        }).start();

        while (!executed.get());

        Assert.assertEquals(ThreadLocalComparatorRegister.getRegisteredNameTypeComparators().size(), 0);


        // Secondary check.
        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(2L, result.getDifferences().size());
    }
}
