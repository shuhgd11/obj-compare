package org.smartboot.compare;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

/**
 * @author qinluo
 * @date 2024-03-20 23:39:24
 * @since 1.0.7
 */
public class NamesType extends NameType {

    protected final List<String> names = new ArrayList<>(8);

    public NamesType(Class<?> type, String ...names) {
        super(null, type);
        if (names == null || names.length == 0) {
            throw new IllegalArgumentException("names must not be empty");
        }
        this.names.addAll(Arrays.asList(names));
    }

    @Override
    public int hashCode() {
        return names.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NamesType)) {
            return false;
        }

        NamesType nt = (NamesType)obj;
        return Objects.equals(nt.names, this.names) && nt.type == this.type;
    }

    @Override
    public String toString() {
        return "{" +
                "names=" + names +
                ", type=" + type +
                '}';
    }

    /**
     * 1、match any name in names.
     * 2、match any subclass of type.
     */
    @Override
    public boolean match(NameType nameType) {
        return this.names.contains(nameType.name)
                && this.type.isAssignableFrom(nameType.type);
    }

    public static NamesType of(Class<?> type, String ...names) {
        return new NamesType(type, names);
    }
}
