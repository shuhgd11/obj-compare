package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.difference.Difference;
import org.smartboot.compare.difference.SizeDifference;
import org.smartboot.compare.difference.TypeDifference;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author qinluo
 * @date 2024-03-20 23:54:44
 * @since 1.0.7
 */
public class MapCompareTest {

    @Test
    public void testCompareMap() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareMapWithLooseMode() {
        Map<String, Integer> expect = new HashMap<>();

        // Loose mode.
        CompareResult result = CompareHelper.compare(expect, null, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        // Strict mode.
        result = CompareHelper.compare(expect, null);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareMapWithImmediatelyInterrupt() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);
        expect.put("d", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);
        actual.put("d", 5);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        result = CompareHelper.compare(expect, actual, Option.mix(Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareMapWithImmediatelyInterrupt02() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);
        actual.put("d", 5);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 3);

        // 因size大小直接中断
        result = CompareHelper.compare(expect, actual, Option.mix(Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
        Assert.assertEquals(result.getDifferences(SizeDifference.class).size(), 1);
    }

    @Test
    public void testCompareMapWithCustomizedComparator() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);

        // 注册一个自定义比较器, 让key为c的对应值，不管怎么样，都返回一致
        ComparatorRegister.register(NameType.of(Integer.class, "c"), new AbstractComparator<Integer>() {
            @Override
            public Difference compare(Integer expect, Integer actual, ComparatorContext<Integer> context) {
                return Difference.SAME;
            }
        });

        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        // 取消注册
        ComparatorRegister.unregister(NameType.of(Integer.class, "c"));

        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testCompareMapWithCustomizedComparator02() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);
        expect.put("d", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);
        actual.put("d", 4);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        // 注册一个自定义比较器, 让key为c的对应值，不管怎么样，都返回一致
        ComparatorRegister.register(NamesType.of(Integer.class, "c", "d"), new AbstractComparator<Integer>() {
            @Override
            public Difference compare(Integer expect, Integer actual, ComparatorContext<Integer> context) {
                return Difference.SAME;
            }
        });

        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        // 取消注册
        ComparatorRegister.unregister(NamesType.of(Integer.class, "c", "d"));

        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testCompareMapIgnoreFields() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);

        // 忽略key为c的值比较
        List<IgnoreField> ignoreFields = new ArrayList<>();
        ignoreFields.add(new IgnoreField("c", Integer.class));

        result = CompareHelper.compare(expect, actual, ignoreFields);
        System.out.println(result);
        Assert.assertTrue(result.isSame());
        Assert.assertEquals(1, result.getSkippedFields().size());
    }

    @Test
    public void testCompareMapWithDifferenceObject() {
        Map<String, Object> expect = new HashMap<>();
        Cat c = new Cat();
        c.name = "Lucy";
        c.type = "lihuamao";
        expect.put("a", c);

        Map<String, Object> actual = new HashMap<>();
        actual.put("a", new User());

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
        Assert.assertEquals(result.getDifferences(TypeDifference.class).size(), 1);
    }

    @Test
    public void testCompareMapUseActualType() {
        Map<String, Object> expect = new HashMap<>();
        Cat c = new Cat();
        c.name = "Lucy";
        c.type = "lihuamao";
        expect.put("a", c);

        Animal animal = new Animal();
        animal.type = "China Cat";
        Map<String, Object> actual = new HashMap<>();
        actual.put("a", animal);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
        Assert.assertEquals(result.getDifferences(TypeDifference.class).size(), 1);

        ComparatorRegister.register(NameType.of(Animal.class, "a"), new AbstractComparator<Animal>() {
            @Override
            public Difference compare(Animal expect, Animal actual, ComparatorContext<Animal> context) {
                if (Objects.equals(expect.type, actual.type)) {
                    return null;
                }
                return new SourceDifference(this, context.getPath(), expect.type, actual.type);
            }
        });

        result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
        Assert.assertEquals(result.getDifferences(SourceDifference.class).size(), 1);
    }

    @Test
    public void testCompareMapUseExpectType() {
        Map<String, Object> expect = new HashMap<>();
        Cat c = new Cat();
        c.name = "Lucy";
        c.type = "lihuamao";
        expect.put("a", c);
        expect.put("b", null);

        Map<String, Object> actual = new HashMap<>();
        actual.put("a", new User());
        actual.put("b", null);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
        Assert.assertEquals(result.getDifferences(TypeDifference.class).size(), 1);

        ComparatorRegister.register(NameType.of(Object.class, "a"), new AbstractComparator<Object>() {
            @Override
            public Difference compare(Object expect, Object actual, ComparatorContext<Object> context) {
                context.addMessage("Use customized Object comparator return null");
                return null;
            }
        });

        result = CompareHelper.compare(expect, actual, Option.mix(Option.USE_EXPECT_TYPE_IN_MAP));
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }

    class Animal {
        String type;
    }

    class Cat extends Animal{
        private String name;
    }
}
