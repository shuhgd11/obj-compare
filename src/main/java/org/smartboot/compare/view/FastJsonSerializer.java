package org.smartboot.compare.view;


import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.serializer.SerializerFeature;

/**
 * @author qinluo
 * @date 2024-04-20 21:46:08
 * @since 1.0.9
 */
public class FastJsonSerializer implements JsonSerializer {

    @Override
    public String toJson(Object obj) {
        return JSON.toJSONString(obj, SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue);
    }
}
