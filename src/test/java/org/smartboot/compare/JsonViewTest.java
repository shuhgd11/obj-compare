package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.difference.Difference;
import org.smartboot.compare.difference.SizeDifference;
import org.smartboot.compare.view.GsonSerializer;
import org.smartboot.compare.view.JsonResultViewer;
import org.smartboot.compare.view.JsonSerializer;
import org.smartboot.compare.view.SimplifyJsonResultViewer;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author qinluo
 * @date 2024-04-20 21:53:08
 * @since 1.0.0
 */
public class JsonViewTest {

    @Test
    public void testSerializeWithFastJson() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", 2);
        expect.put("c", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);
        actual.put("d", 5);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(new SimplifyJsonResultViewer(result));
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 3);
        Assert.assertEquals(result.getDifferences(SizeDifference.class).size(), 1);
    }

    @Test
    public void testSerializeWithGson() {
        JsonSerializer.setDefaultInstance(new GsonSerializer());

        Map<String, Object> expect = new HashMap<>();
        expect.put("a", 1);
        expect.put("b", "123456888");
        expect.put("c", "hhaks");

        Map<String, Object> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", "1234567890");
        actual.put("c", "hhaklsa");
        actual.put("d", 5);

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(new JsonResultViewer(result));
        JsonSerializer.setDefaultInstance(null);

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 4);
        Assert.assertEquals(result.getDifferences(SizeDifference.class).size(), 1);
    }

    @Test
    public void testSerializeUnknownDifference() {
        Map<String, Integer> expect = new HashMap<>();
        expect.put("a", 2);
        expect.put("b", 2);
        expect.put("c", 3);

        Map<String, Integer> actual = new HashMap<>();
        actual.put("a", 1);
        actual.put("b", 2);
        actual.put("c", 4);
        actual.put("d", 5);

        ThreadLocalComparatorRegister.register(NameType.of(Integer.class, "a"), new AbstractComparator<Integer>() {
            @Override
            public Difference compare(Integer expect, Integer actual, ComparatorContext<Integer> context) {
                if (Objects.equals(expect, actual)) {
                    return null;
                }

                return new Difference() {
                    @Override
                    public String getMessage() {
                        return "left = " + expect + ", right = " + actual;
                    }

                    @Override
                    public String type() {
                        return "Unknown";
                    }
                };

            }
        });

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(new JsonResultViewer(result));

        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 4);
        Assert.assertEquals(result.getDifferences(SizeDifference.class).size(), 1);
    }
}
