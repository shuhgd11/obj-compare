package org.smartboot.compare;

import java.util.ArrayList;
import java.util.List;

/**
 * Object recycle checker.
 *
 * @author qinluo
 * @version 1.0.0
 * @date 2021/2/4 12:11:52
 */
public class RecycleChecker {

    private final List<RecycleCache> caches = new ArrayList<>(36);


    /**
     * Add recycle cache for recycle check.
     *
     * @param expect expect
     * @param actual actual
     * @param level  depth level
     */
    public void addRecycle(Object expect, Object actual, int level) {
        caches.add(new RecycleCache(expect, actual, level));
    }

    /**
     * Check expect and actual object is recycle.
     *
     * @param expect expect
     * @param actual actual
     * @param level  depth level
     * @return       recycle check result;
     */
    public boolean isRecycle(Object expect, Object actual, int level) {
        for (RecycleCache cache : caches) {
            if (cache.isMatch(expect, actual, level)) {
                return true;
            }
        }

        return false;
    }


    /**
     * Recycle cache.
     */
    private static class RecycleCache {

        /**
         * expect.
         */
        private final Object expect;

        /**
         * actual.
         */
        private final Object actual;

        /**
         * recycle level.
         */
        private final int level;

        RecycleCache(Object expect, Object actual, int level) {
            this.expect = expect;
            this.actual = actual;
            this.level = level;
        }

        public boolean isMatch(Object expect, Object actual, int level) {
            return this.level != level &&
                    (this.expect == expect && this.actual == actual) || (this.expect == actual && this.actual == expect);
        }
    }

}
