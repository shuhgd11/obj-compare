package org.smartboot.compare.comparator;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.Option;
import org.smartboot.compare.difference.Difference;
import org.smartboot.compare.difference.BaseDifference;
import org.smartboot.compare.utils.ComparatorUtils;

import java.util.Objects;

/**
 * @author qinluo
 * @version 1.0.0
 * @since 2019-05-27 17:22
 */
public class BooleanComparator extends AbstractComparator<Boolean> {

    @Override
    public Difference compare(Boolean expect, Boolean actual, ComparatorContext<Boolean> context) {
        if (context.hasOption(Option.LOOSE_MODE) && ComparatorUtils.checkFalse(expect, actual)) {
            return Difference.SAME;
        }

        if (!Objects.equals(expect, actual)) {
            return new BaseDifference(context.getPath(), expect, actual);
        }

        return Difference.SAME;
    }
}
