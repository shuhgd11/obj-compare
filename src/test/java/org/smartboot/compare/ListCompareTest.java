package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author qinluo
 * @date 2024-03-20 23:54:44
 * @since 1.0.7
 */
public class ListCompareTest {

    @Test
    public void testCompareList() {
        List<String> expect = new ArrayList<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");

        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testCompareListWithLooseMode() {
        List<String> expect = new ArrayList<>();

        // Loose mode.
        CompareResult result = CompareHelper.compare(expect, null, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        // Strict mode.
        result = CompareHelper.compare(expect, null);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareListWithTypeDiff() {
        List<String> expect = new ArrayList<>();
        List<String> actual = new LinkedList<>();

        // Loose mode.
        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        // Strict mode.
        result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareListWithImmediatelyInterrupt() {
        List<String> expect = new ArrayList<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");
        expect.add("d");
        expect.add("f");

        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.add("c");
        actual.add("c");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        // index 3和4都不一致，但比较3时失败中断比较
        result = CompareHelper.compare(expect, actual, Option.mix(Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareListWithTransAsSet() {
        List<String> expect = new ArrayList<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");
        expect.add("b");
        expect.add("a");

        List<String> actual = new ArrayList<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.add("c");
        actual.add("c");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        result = CompareHelper.compare(expect, actual, Option.mix(Option.TRANS_AS_SET));
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }
}
