package org.smartboot.compare.constants;

/**
 * @author qinluo
 * @date 2022-03-11 18:44:10
 * @since 1.0.0
 */
public enum Kind {

    /**
     * JAVA_LANG_FIELD
     */
    FIELD,

    /**
     * Key of map, index of List.
     */
    KEY,

}
