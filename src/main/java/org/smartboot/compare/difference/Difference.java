package org.smartboot.compare.difference;

import org.smartboot.compare.comparator.Comparator;

import java.util.Map;

/**
 *
 * Compare difference item messages.
 *
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 15:33
 */
public interface Difference {

    /**
     * It's means equals.
     */
    Difference SAME = null;

    /**
     * Get compare difference message.
     *
     * @return message.
     */
    String getMessage();

    /**
     * Difference type.
     * @return type string.
     */
    String type();

    /**
     * Returns json view model.
     *
     * @see org.smartboot.compare.view.JsonResultViewer
     */
    default Map<String, Object> getJsonViewModel() {
        return null;
    }

    /**
     * Return attach comparator source.
     *
     * @since 1.0.9.1
     * @return attached source.
     */
    default Comparator<?> getSource() {
        return null;
    }

    /**
     * Attach a comparator with this object.
     *
     * @since 1.0.9.1
     * @param source source
     */
    default void attachSource(Comparator<?> source) {
    }

}
