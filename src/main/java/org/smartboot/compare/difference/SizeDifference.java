package org.smartboot.compare.difference;

import org.smartboot.compare.Path;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 16:19
 */
public class SizeDifference extends BaseDifference {

    protected final Object expectObject;
    protected final Object actualObject;

    public SizeDifference(Path path, int expectValSize, int compareValSize,
                          Object expectObject, Object actualObject) {
        super(path, expectValSize, compareValSize);
        this.actualObject = actualObject;
        this.expectObject = expectObject;
    }

    public Object getExpectObject() {
        return expectObject;
    }

    public Object getActualObject() {
        return actualObject;
    }

    @Override
    protected String expectDesc() {
        return "期望大小";
    }

    @Override
    protected String actualDesc() {
        return "实际大小";
    }

    @Override
    public String type() {
        return "Size";
    }
}
