
package org.smartboot.compare.array;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.constants.CompareConstants;
import org.smartboot.compare.difference.Difference;

/**
 * @author qinluo
 * @date 2024-03-24 01:40:51
 * @since 1.0.7
 */
public class ByteArrayComparator extends AbstractComparator<Byte[]> {

    @Override
    public Difference compare(Byte[] expect, Byte[] actual, ComparatorContext<Byte[]> context) {
        return ArrayHelper.compare(expect, actual, context, CompareConstants.WRAPPER);
    }
}
