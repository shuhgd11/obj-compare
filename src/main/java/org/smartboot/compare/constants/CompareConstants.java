package org.smartboot.compare.constants;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2021/2/4 1:08 上午
 */
public class CompareConstants {

    /**
     * The Maximum compare depth.
     */
    public static int MAX_COMP_DEPTH = 10;

    /**
     * The Maximum size with {@link org.smartboot.compare.Option#TRANS_AS_SET}
     */
    public static int MAX_TRANS_SIZE = 1024;

    /**
     * Prefix path in map.
     */
    public static final String KEY_PREFIX = "(key)";

    /**
     * Primitive array.
     */
    public static final int PRIMITIVE = 0;

    /**
     * Wrapper array.
     */
    public static final int WRAPPER = 1;

    /**
     * Object array
     */
    public static final int OBJECT = 3;

    /**
     * List
     */
    public static final int LIST = 4;

    /**
     * Building array index path.
     *
     * @param index array/list index.
     * @return      [index]
     */
    public static String arrayIndex(int index) {
        return "[" + index + "]";
    }

    /**
     * Check max has over {@link CompareConstants#MAX_TRANS_SIZE}
     *
     * @param max max
     * @return    limitless
     */
    public static boolean limitless(int max) {
        return MAX_TRANS_SIZE == -1 || MAX_TRANS_SIZE >= max;
    }
}
