package org.smartboot.compare.comparator;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.difference.Difference;
import org.smartboot.compare.difference.NullOfOneObject;
import org.smartboot.compare.difference.BaseDifference;

import java.util.Date;
import java.util.Objects;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-05-15 16:58
 */
public class DateComparator extends AbstractComparator<Date> {

    @Override
    public Difference compare(Date expect, Date actual, ComparatorContext<Date> context) {
        if (expect == null || actual == null) {
            return new NullOfOneObject(context.getPath(), expect, actual);
        }

        if (!Objects.equals(expect, actual)) {
            return new BaseDifference(context.getPath(), expect, actual);
        }

        return Difference.SAME;
    }
}
