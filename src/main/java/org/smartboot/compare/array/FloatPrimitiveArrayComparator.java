

package org.smartboot.compare.array;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.constants.CompareConstants;
import org.smartboot.compare.difference.Difference;

/**
 * @author qinluo
 * @date 2024-03-24 01:40:51
 * @since 1.0.7
 */
public class FloatPrimitiveArrayComparator extends AbstractComparator<float[]> {

    @Override
    public Difference compare(float[] expect, float[] actual, ComparatorContext<float[]> context) {
        return ArrayHelper.compare(expect, actual, context, CompareConstants.PRIMITIVE);
    }
}
