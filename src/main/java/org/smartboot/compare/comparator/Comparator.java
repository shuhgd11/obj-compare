package org.smartboot.compare.comparator;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.difference.Difference;

/**
 * @author qinluo
 * @version 1.0.0
 * @since 2019-05-27 16:48
 */
public interface Comparator<T> {

    /**
     * 比较2个对象是否相等
     *
     * @param context 比较上下文，承载比较结果
     * @return        differences
     */
    Difference compare(ComparatorContext<T> context);

    /**
     * Generator unique identifier for this comparator.
     *
     * @see org.smartboot.compare.difference.BaseDifference
     * @since 1.0.9.1
     * @return identifier
     */
    default String identifier() {
        return this.getClass().getName();
    }
}
