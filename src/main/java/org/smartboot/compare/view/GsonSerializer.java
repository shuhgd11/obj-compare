package org.smartboot.compare.view;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * @author qinluo
 * @date 2024-04-20 21:59:18
 * @since 1.0.0
 */
public class GsonSerializer implements JsonSerializer {

    public static final Gson GSON_PRETTY = new GsonBuilder().setPrettyPrinting().serializeNulls().create();


    @Override
    public String toJson(Object obj) {
        return GSON_PRETTY.toJson(obj);
    }
}
