package org.smartboot.compare.view;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONWriter.Feature;

/**
 * @author qinluo
 * @date 2024-04-20 21:46:08
 * @since 1.0.9
 */
public class FastJson2Serializer implements JsonSerializer {

    @Override
    public String toJson(Object obj) {
        return JSON.toJSONString(obj, Feature.PrettyFormat, Feature.WriteMapNullValue);
    }
}
