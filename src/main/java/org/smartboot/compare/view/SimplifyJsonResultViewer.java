package org.smartboot.compare.view;

import org.smartboot.compare.CompareResult;
import org.smartboot.compare.difference.Difference;

import java.util.Map;

/**
 * @author qinluo
 * @date 2024-06-07 08:35:16
 * @since 1.1.0
 */
public class SimplifyJsonResultViewer extends JsonResultViewer {

    public SimplifyJsonResultViewer(CompareResult result) {
        super(result);
    }

    @Override
    protected void adjustViewModel() {
        Object r0 = this.get(DETAIL);
        Object r1 = this.get(RESULT);

        this.viewModel.clear();

        // Only Reserved details and result.
        this.viewModel.put(DETAIL, r0);
        this.viewModel.put(RESULT, r1);
    }

    @Override
    protected Map<String, Object> adjustJsonViewModel(Difference difference, Map<String, Object> jsonViewModel) {
        if (jsonViewModel != null) {
            jsonViewModel.remove("type");
            jsonViewModel.remove("source");
        }

        return jsonViewModel;
    }
}
