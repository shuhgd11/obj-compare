package org.smartboot.compare;

import org.junit.Test;
import org.smartboot.compare.constants.Kind;

/**
 * @author qinluo
 * @date 2022-03-10 17:54:54
 * @since 1.0.0
 */
public class FieldFilterTest {

    @Test
    public void testFilter() {
        /* load from database */
        Item expect = new Item();
        expect.setId(1L);
        expect.setName("无常大米");
        expect.setUrl("https://www.baidu.com/1.jpg");
        expect.setStock(10);

        /* modified with snapshoted */
        Item actual = new Item();
        actual.setId(1L);
        actual.setName("无常大米2");
        actual.setUrl("https://www.baidu.com/1.jpg");
        actual.setStock(10);

        /* Register global field filter : filter field not contains @Snapshoted */
        FieldFilter filter = (f, context, curPath) -> f.getType() == Kind.FIELD && f.getField().getAnnotation(Snapshoted.class) == null;

        FieldFilters.registerGlobal(filter);

        CompareResult result = CompareHelper.compare(expect, actual);

        if (!result.isSame()) {
            System.out.println(result);
        }

        /* modified without snapshoted */
        actual.setName("无常大米");
        actual.setStock(10);

        System.out.println("================================");

        result = CompareHelper.compare(expect, actual);
        System.out.println(result);

        FieldFilters.removeGlobal(filter);
    }
}
