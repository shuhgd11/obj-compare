package org.smartboot.compare;

import org.junit.Test;
import org.smartboot.compare.view.SimplifyJsonResultViewer;

/**
 * @author qinluo
 * @date 2024-03-24 02:34:35
 * @since 1.0.0
 */
public class ArrayComparatorTest {

    @Test
    public void testCompareInt() {
        int[] expect = new int[10];
        int[] actual = new int[10];
        for (int i = 0; i < 10; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
    }

    @Test
    public void testCompareInt2() {
        int[] expect = new int[10];
        int[] actual = new int[10];
        for (int i = 0; i < 5; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.BEAUTIFUL_ARRAY_RESULT));
        System.out.println(result);
    }

    @Test
    public void testCompareInt3() {
        int[] expect = new int[10];
        int[] actual = new int[10];
        for (int i = 0; i < 10; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.TRANS_AS_SET));
        System.out.println(result);
    }

    @Test
    public void testCompareInt4() {
        int[] expect = new int[12];
        int[] actual = new int[10];
        for (int i = 0; i < 10; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.TRANS_AS_SET));
        System.out.println(result);
    }

    @Test
    public void testCompareInt5() {
        int[] expect = new int[12];
        int[] actual = new int[13];
        for (int i = 0; i < 10; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        expect[10] = 99;
        actual[11] = 88;

        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.TRANS_AS_SET));
        System.out.println(result);
    }


    @Test
    public void testCompareDouble() {
        double[] expect = new double[12];
        double[] actual = new double[10];
        for (int i = 0; i < 10; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        expect[10] = 99;

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
    }


    @Test
    public void testCompareFloat() {
        float[] expect = new float[10];
        float[] actual = new float[10];
        for (int i = 0; i < 10; i++) {
            expect[i] = i;
            actual[i] = 10 - i - 1;
        }

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
    }


    @Test
    public void testCompareObj() {
        Object[] expect = new Object[10];
        Object[] actual = new Object[10];
        for (int i = 0; i < 10; i++) {
            expect[i] = String.valueOf(i);
            actual[i] = i;
        }

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
    }

    @Test
    public void testCompareObjectArray() {
        Object[] expect = new Object[1];
        Object[] actual = new Object[1];


        User expectU = new User();
        expectU.setId("1");
        expectU.setPassword("111212");
        expectU.setUsername("Tom");
        expectU.setSex("male");

        User targetU = new User();
        targetU.setId("2");
        targetU.setPassword("1112112");
        targetU.setUsername("Jerry");
        targetU.setSex("male");

        expect[0] = expectU;
        actual[0] = targetU;

        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.DISABLE_EQUALS, Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
    }

    @Test
    public void testSizeNotEquals() {
        int[] expect = new int[] {1,2,3};
        int[] actual = new int[] {1,2,3,4,5};
        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        System.out.println(new SimplifyJsonResultViewer(result));
    }

    @Test
    public void testTypeUnmatched() {
        Object[] expect = new Object[2] ;
        Object[] actual = new Object[2];

        expect[0] = "avc";
        actual[0] = 111L;

        expect[1] = new int[] {1,2,3};
        actual[1] = new int[] {1,4,6};


        CompareResult result = CompareHelper.compare(expect, actual, Option.mix(Option.TRANS_AS_SET));
        System.out.println(result);
        System.out.println(new SimplifyJsonResultViewer(result));
    }
}
