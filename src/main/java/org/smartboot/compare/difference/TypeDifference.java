package org.smartboot.compare.difference;

import org.smartboot.compare.Path;

import java.util.Map;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 16:35
 */
public class TypeDifference extends BaseDifference {

    public TypeDifference(Path path, Object expectVal, Object compareVal) {
        super(path, expectVal, compareVal);
    }

    protected String cast(Object val) {
        if (val != null) {
            return val.getClass().getName() + ", " + val;
        }

        return "null";
    }

    @Override
    public String type() {
        return "TypeUnmatched";
    }

    @Override
    public Map<String, Object> getJsonViewModel() {
        Map<String, Object> jsonViewModel = super.getJsonViewModel();
        jsonViewModel.put("expect_java_type", expect != null ? expect.getClass().getName() : null);
        jsonViewModel.put("actual_java_type", actual != null ? actual.getClass().getName() : null);

        return jsonViewModel;
    }
}
