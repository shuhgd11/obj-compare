package org.smartboot.compare;

/**
 * @author qinluo
 * @date 2022-03-10 17:42:04
 * @since 1.0.0
 */
public interface FieldFilter {

    /**
     * filter some field.
     *
     * @param f       field.
     * @param context context.
     * @param curPath current path.
     * @return        filtered.
     */
    boolean filtered(FieldCache f, ComparatorContext<?> context, Path curPath);
}
