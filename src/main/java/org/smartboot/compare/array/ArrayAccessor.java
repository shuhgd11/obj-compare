package org.smartboot.compare.array;

import org.smartboot.compare.Accessor;
import org.smartboot.compare.Option;
import org.smartboot.compare.constants.CompareConstants;

import java.lang.reflect.Array;

/**
 * @author qinluo
 * @date 2024-06-08 18:14:47
 * @since 1.1.0
 */
public class ArrayAccessor implements Accessor {

    private final Object array;
    private final int length;
    private final int type;

    public ArrayAccessor(Object arrayObject, int type) {
        this.array = arrayObject;
        this.length = ArrayHelper.getLength(this.array);
        this.type = type;
    }

    @Override
    public int getLength() {
        return length;
    }

    @Override
    public Object elementAt(int index) {
        return Array.get(array, index);
    }

    @Override
    public Object getSource() {
        return array;
    }

    @Override
    public boolean supportOption(Option option) {
        if (option == Option.TRANS_AS_SET) {
            return type != CompareConstants.OBJECT;
        } else if(option == Option.BEAUTIFUL_ARRAY_RESULT) {
            return type != CompareConstants.OBJECT;
        }

        return true;
    }

    @Override
    public int getSourceType() {
        return type;
    }
}
