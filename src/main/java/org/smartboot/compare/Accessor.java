package org.smartboot.compare;

/**
 * @author qinluo
 * @date 2024-06-08 18:09:19
 * @since 1.1.0
 */
public interface Accessor {

    /**
     * Return array/list size.
     *
     * @return size
     */
    int getLength();

    /**
     * Return element at index.
     *
     * @param index index.
     * @return      element.
     */
    Object elementAt(int index);

    /**
     * Return source object of this accessor.
     *
     * @return source object.
     */
    Object getSource();

    /**
     * Return option supported.
     *
     * @param option option.
     * @return       supported.
     */
    boolean supportOption(Option option);

    /**
     * Return source object type.
     *
     * @see org.smartboot.compare.constants.CompareConstants#PRIMITIVE
     * @see org.smartboot.compare.constants.CompareConstants#WRAPPER
     * @see org.smartboot.compare.constants.CompareConstants#OBJECT
     * @see org.smartboot.compare.constants.CompareConstants#LIST
     *
     * @return type
     */
    int getSourceType();
}
