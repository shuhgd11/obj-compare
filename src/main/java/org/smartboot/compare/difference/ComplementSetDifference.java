package org.smartboot.compare.difference;

import org.smartboot.compare.Path;

import java.util.Map;
import java.util.Set;

/**
 * 数组/List/Set/Map之间的补集差异
 *
 * @author qinluo
 * @date 2024-03-26 23:44:48
 * @since 1.0.7
 */
public class ComplementSetDifference extends BaseDifference {

    public ComplementSetDifference(Path path, Object expect, Object actual) {
        super(path, expect, actual);
    }

    @Override
    protected String expectDesc() {
        return hasElement(expect) ? "期望集合中不存在" : null;
    }

    @Override
    protected String actualDesc() {
        return hasElement(actual) ? "对比集合中不存在" : null;
    }

    private boolean hasElement(Object value) {
        if (value == null) {
            return false;
        }
        Set objects = (Set) value;
        return !objects.isEmpty();
    }

    @Override
    public String type() {
        return "ComplementSet";
    }

    @Override
    public Map<String, Object> getJsonViewModel() {
        Map<String, Object> jsonViewModel = super.getJsonViewModel();
        jsonViewModel.put("NotExistInExpect", jsonViewModel.remove("expect"));
        jsonViewModel.put("NotExistInActual", jsonViewModel.remove("actual"));

        return jsonViewModel;
    }
}
