package org.smartboot.compare;

import org.smartboot.compare.comparator.Comparator;
import org.smartboot.compare.difference.BaseDifference;

/**
 * @author qinluo
 * @date 2024-03-23 23:40:47
 * @since 1.0.0
 */
public class SourceDifference extends BaseDifference {

    private final Comparator comparator;

    public SourceDifference(Comparator source, Path path, Object expect, Object actual) {
        super(path, expect, actual);
        this.comparator = source;
    }

    public Comparator getComparator() {
        return comparator;
    }

    @Override
    public String type() {
        return "BasedWithSource";
    }
}
