package org.smartboot.compare.difference;

import org.smartboot.compare.Path;
import org.smartboot.compare.comparator.Comparator;
import org.smartboot.compare.utils.ArgumentUtils;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 16:10
 */
public abstract class AbstractDifference implements Difference {

    /**
     * Compare path.
     */
    protected final Path path;

    /**
     * The comparator source that generated this object.
     */
    protected Comparator<?> source;

    public AbstractDifference(Path path) {
        ArgumentUtils.notNull(path, "path must not be null!");
        this.path = path;
    }

    @Override
    public String toString() {
        return getMessage();
    }

    public String getPath() {
        return path.getFullPath();
    }

    public Path getPathObject() {
        return path;
    }

    @Override
    public Comparator<?> getSource() {
        return source;
    }

    @Override
    public void attachSource(Comparator<?> source) {
        this.source = source;
    }
}
