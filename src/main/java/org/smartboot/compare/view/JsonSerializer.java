package org.smartboot.compare.view;

import java.util.concurrent.atomic.AtomicReference;

/**
 * @author qinluo
 * @date 2024-04-20 21:40:36
 * @since 1.0.9
 */
public interface JsonSerializer {

    AtomicReference<JsonSerializer> INSTANCE = new AtomicReference<>();

    /**
     * Convert object to json string.
     *
     * @param obj object.
     * @return    json string.
     */
    String toJson(Object obj);

    static JsonSerializer getInstance() {
        return INSTANCE.get();
    }

    static void setDefaultInstance(JsonSerializer serializer) {
        INSTANCE.set(serializer);
    }
}
