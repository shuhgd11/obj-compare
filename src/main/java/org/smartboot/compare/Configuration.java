package org.smartboot.compare;

/**
 * @author qinluo
 * @date 2024-07-14 20:57:57
 * @since 1.1.1
 */
public interface Configuration {

    /**
     * Convert this configuration object as subtype configuration
     *
     * @param <T> dest subclass
     * @return this
     */
    default <T extends Configuration> T asSubConfiguration() {
        return (T) this;
    }
}
