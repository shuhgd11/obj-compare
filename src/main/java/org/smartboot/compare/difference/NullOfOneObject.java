package org.smartboot.compare.difference;

import org.smartboot.compare.Path;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 16:18
 */
public class NullOfOneObject extends BaseDifference {

    public NullOfOneObject(Path path, Object expectVal, Object compareVal) {
        super(path, expectVal, compareVal);
    }

    @Override
    public String type() {
        return "NullOfOne";
    }
}
