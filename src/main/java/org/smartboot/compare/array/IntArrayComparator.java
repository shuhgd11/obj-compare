package org.smartboot.compare.array;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.constants.CompareConstants;
import org.smartboot.compare.difference.Difference;

/**
 * @author qinluo
 * @date 2024-03-18 23:07:34
 * @since 1.0.7
 */
public class IntArrayComparator extends AbstractComparator<int[]> {

    @Override
    public Difference compare(int[] expect, int[] actual, ComparatorContext<int[]> context) {
        return ArrayHelper.compare(expect, actual, context, CompareConstants.PRIMITIVE);
    }
}
