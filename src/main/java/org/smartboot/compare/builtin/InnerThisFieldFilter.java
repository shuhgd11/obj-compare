package org.smartboot.compare.builtin;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.FieldCache;
import org.smartboot.compare.FieldFilter;
import org.smartboot.compare.Path;
import org.smartboot.compare.constants.Kind;

import java.util.Objects;

/**
 * Filter inner class this$0
 *
 * @author qinluo
 * @date 2022-03-10 17:45:24
 * @since 1.0.0
 */
public class InnerThisFieldFilter implements FieldFilter {

    @Override
    public boolean filtered(FieldCache f, ComparatorContext<?> context, Path curPath) {
        // 忽略内部类中的this$0变量
        return f.getType() == Kind.FIELD && Objects.equals(f.getField().getName(), "this$0");
    }
}
