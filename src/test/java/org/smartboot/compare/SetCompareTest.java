package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * @author qinluo
 * @date 2024-03-20 23:54:44
 * @since 1.0.7
 */
public class SetCompareTest {

    @Test
    public void testCompareSet() {
        Set<String> expect = new HashSet<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");

        Set<String> actual = new HashSet<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testCompareSetWithLooseMode() {
        Set<String> expect = new HashSet<>();

        // Loose mode.
        CompareResult result = CompareHelper.compare(expect, null, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        // Strict mode.
        result = CompareHelper.compare(expect, null);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareSetWithImmediatelyInterrupt() {
        Set<String> expect = new HashSet<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");
        expect.add("d");

        Set<String> actual = new HashSet<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        // 因size大小直接中断
        result = CompareHelper.compare(expect, actual, Option.mix(Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }

    @Test
    public void testCompareSetWithImmediatelyInterrupt02() {
        Set<String> expect = new HashSet<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");
        expect.add("d");
        expect.add("h");

        Set<String> actual = new HashSet<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.add("e");
        actual.add("g");
        actual.add("i");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        result = CompareHelper.compare(expect, actual, Option.mix(Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }


    @Test
    public void testCompareSetWithImmediatelyInterrupt2() {
        Set<String> expect = new HashSet<>();
        expect.add("a");
        expect.add("b");
        expect.add("c");

        Set<String> actual = new HashSet<>();
        actual.add("a");
        actual.add("b");
        actual.add("c");
        actual.add("d");

        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 2);

        // 因size大小直接中断
        result = CompareHelper.compare(expect, actual, Option.mix(Option.IMMEDIATELY_INTERRUPT));
        System.out.println(result);
        Assert.assertFalse(result.isSame());
        Assert.assertEquals(result.getDifferences().size(), 1);
    }
}
