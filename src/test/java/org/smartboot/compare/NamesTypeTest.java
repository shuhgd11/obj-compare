package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @author qinluo
 * @date 2024-04-04 12:10:42
 * @since 1.0.8
 */
public class NamesTypeTest {

    @Test
    public void testPutMap() {
        NamesType type = NamesType.of(Object.class, "a", "b");

        Map<NamesType, Integer> map = new HashMap<>();

        map.put(type, 1);

        System.out.println(map);

        Assert.assertEquals((Integer) 1, map.get(NamesType.of(Object.class, "a", "b")));
    }
}
