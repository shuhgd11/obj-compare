package org.smartboot.compare.comparator;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.array.ArrayHelper;
import org.smartboot.compare.difference.Difference;

import java.util.List;

/**
 * @author qinluo
 * @version 1.0.0
 * @since 2019-05-27 17:31
 */
public class ListComparator extends AbstractComparator<List<?>> {

    @Override
    public Difference compare(List<?> expect, List<?> actual, ComparatorContext<List<?>> context) {
        ListAccessor expectAccessor = new ListAccessor(expect);
        ListAccessor actualAccessor = new ListAccessor(actual);
        return ArrayHelper.compare(expectAccessor, actualAccessor, context);
    }

}
