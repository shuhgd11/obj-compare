package org.smartboot.compare.difference;

import org.smartboot.compare.Path;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 16:38
 */
public class DifferenceError extends AbstractDifference {

    private final Exception exception;

    public DifferenceError(Path path, Exception e) {
        super(path);
        this.exception = e;
    }

    public Exception getException() {
        return exception;
    }

    @Override
    public String getMessage() {
        return "Error, " + path + ", cause = " + exception;
    }

    @Override
    public String type() {
        return "Error";
    }
}
