package org.smartboot.compare;

import org.smartboot.compare.difference.Difference;
import org.smartboot.compare.utils.ArgumentUtils;

import java.util.List;

/**
 * @author qinluo
 * @version 1.0.0
 * @since 2019-05-27 16:23
 */
public class CompareHelper {

    public static CompareResult compare(ComparatorContext<Object> ctx) {
        ArgumentUtils.notNull(ctx, "ctx must not be null");

        long start = System.currentTimeMillis();
        Difference difference = ComparatorRegister.dispatcherComparator().compare(ctx);
        long end = System.currentTimeMillis();

        CompareResult result = new CompareResult();
        result.setEscaped((end - start));
        result.setOptions(ctx.getOptions());
        result.setMaxDepth(ctx.getMaxDepth());
        result.addDifference(difference);
        result.setId(ctx.getId());
        result.setRecycleCnt(ctx.getRecycleCnt());
        result.setMessages(ctx.getMessages());
        result.setSkippedFields(ctx.getSkippedFields());
        result.setGroupMessages(ctx.getGroupMessages());

        return result;
    }

    public static CompareResult compare(Object expect, Object actual, List<IgnoreField> ignoreFields, long options, FieldFilter... filters) {
        ComparatorContext<Object> ctx = new ComparatorContext<>(options);
        ctx.setExpect(expect);
        ctx.setActual(actual);
        ctx.setIgnoreFields(ignoreFields);
        ctx.registerFieldFilters(filters);

        return compare(ctx);
    }

    public static CompareResult compare(Object expect, Object actual) {
        return compare(expect, actual, null, 0);
    }

    public static CompareResult compare(Object expect, Object actual, List<IgnoreField> ignoreFields) {
        return compare(expect, actual, ignoreFields, 0);
    }

    public static CompareResult compare(Object expect, Object actual, long options) {
        return compare(expect, actual, null, options);
    }
}


