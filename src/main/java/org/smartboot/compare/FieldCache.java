package org.smartboot.compare;

import org.smartboot.compare.constants.Kind;

import java.lang.reflect.Field;

/**
 * @author qinluo
 * @date 2022-03-11 18:49:02
 * @since 1.0.0
 */
public class FieldCache {

    /**
     * The origin field.
     */
    private Field field;

    /**
     * 字段类型
     */
    private final Kind type;

    /**
     * named type.
     */
    private final NameType nameType;

    public FieldCache(Field field) {
        this.field = field;
        this.field.setAccessible(true);
        this.type = Kind.FIELD;
        this.nameType = NameType.of(field.getType(), field.getName());
    }

    public FieldCache(String name, Class<?> clz) {
        this.type = Kind.KEY;
        this.nameType = NameType.of(clz, name);
    }

    public Field getField() {
        return field;
    }

    public Kind getType() {
        return type;
    }

    public NameType getNamedType() {
        return nameType;
    }
}
