package org.smartboot.compare;

/**
 * @author qinluo
 * @date 2024-07-14 21:00:18
 * @since 1.1.1
 */
public interface FeatureFunction {

    /**
     * The empty feature implement.
     *
     */
    FeatureFunction DEFAULT = new FeatureFunction() {};

    /**
     * Sort given two array object before compare process.
     * @see org.smartboot.compare.constants.CompareConstants#PRIMITIVE
     * @see org.smartboot.compare.constants.CompareConstants#WRAPPER
     * @see org.smartboot.compare.constants.CompareConstants#OBJECT
     * @see org.smartboot.compare.constants.CompareConstants#LIST
     *
     * @param ctx         ctx.
     * @param expectArray expect array.
     * @param actualArray actual array.
     * @param type        type
     */
    default void sort(ComparatorContext<?> ctx, Object expectArray, Object actualArray, int type) {

    }

    /**
     * Convert given string value to other object in default StringComparator behavior.
     *
     * @param ctx   ctx.
     * @param value string value.
     * @return      other object.
     */
    default Object convertString2Object(ComparatorContext<?> ctx, String value) {
        return value;
    }

    /**
     * Create subcontext in sub-compare process.
     *
     * @param ctx parent ctx.
     * @return    subcontext.
     */
    default ComparatorContext<Object> createContext(ComparatorContext<?> ctx) {
        return null;
    }


    /**
     * Check option has effect with current ctx.
     *
     * @param ctx    ctx.
     * @param option option.
     * @return       effected.
     */
    default Boolean isEffectOption(ComparatorContext<?> ctx, Option option) {
        return null;
    }
}
