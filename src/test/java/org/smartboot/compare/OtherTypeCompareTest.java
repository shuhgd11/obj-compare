package org.smartboot.compare;

import org.junit.Assert;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author qinluo
 * @date 2024-03-23 21:19:10
 * @since 1.0.7
 */
public class OtherTypeCompareTest {

    @Test
    public void testCompareDate01() {
        Date expect = new Date();
        Date actual = new Date(expect.getTime());

        // DateComparator
        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testCompareDateOneOfNull() {
        Date expect = new Date();

        // DateComparator
        CompareResult result = CompareHelper.compare(expect, null);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
    }

    @Test
    public void testCompareDateDifference() throws Exception {
        Date expect = new Date();
        Date actual = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse("2021-12-31 11:00:00");

        // DateComparator
        CompareResult result = CompareHelper.compare(expect, actual);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
    }

    @Test
    public void testCompareBoolean01() {
        // BooleanComparator
        CompareResult result = CompareHelper.compare(true, true);
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        result = CompareHelper.compare(false, false);
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        result = CompareHelper.compare(true, false);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
    }

    @Test
    public void testCompareBoolean02() {
        // BooleanComparator
        CompareResult result = CompareHelper.compare(false, null);
        System.out.println(result);
        Assert.assertFalse(result.isSame());

        result = CompareHelper.compare(false, null, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
        Assert.assertTrue(result.isSame());
    }

    @Test
    public void testStringComparator() {
        // StringComparator
        CompareResult result = CompareHelper.compare("hello", "hello");
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        // 宽松模式
        result = CompareHelper.compare("", null, Option.mix(Option.LOOSE_MODE));
        System.out.println(result);
        Assert.assertTrue(result.isSame());

        // 非宽松模式，不相等
        result = CompareHelper.compare("", null);
        System.out.println(result);
        Assert.assertFalse(result.isSame());
    }
}
