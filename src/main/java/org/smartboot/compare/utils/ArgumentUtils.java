package org.smartboot.compare.utils;

/**
 * @author qinluo
 * @date 2024-06-07 20:59:15
 * @since 1.1.0
 */
public final class ArgumentUtils {

    public static void notNull(Object value, String msg) {
        if (value == null) {
            throw new IllegalArgumentException(msg);
        }
    }
}
