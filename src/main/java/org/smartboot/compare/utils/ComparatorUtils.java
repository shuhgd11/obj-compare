package org.smartboot.compare.utils;

import java.util.Collection;
import java.util.Map;

/**
 * @author qinluo
 * @version 1.0.0
 * @since 2019-05-27 23:00
 */
public class ComparatorUtils {

    public static boolean checkBlank(String expect, String actual) {
        return (expect == null || expect.trim().isEmpty()) && (actual == null || actual.trim().isEmpty());
    }

    public static boolean checkEmpty(String expect, String actual) {
        return (expect == null || expect.isEmpty()) && (actual == null || actual.isEmpty());
    }

    public static boolean checkEmpty(Collection<?> expect, Collection<?> actual) {
        return (expect == null || expect.isEmpty()) && (actual == null || actual.isEmpty());
    }

    public static boolean checkEmpty(Map<?, ?> expect, Map<?, ?> actual) {
        return (expect == null || expect.isEmpty()) && (actual == null || actual.isEmpty());
    }

    public static boolean checkFalse(Boolean expect, Boolean actual) {
        return (expect == null || !expect) && (actual == null || !actual);
    }

    public static boolean checkAllNull(Object a, Object b) {
        return a == null && b == null;
    }
}
