
package org.smartboot.compare.array;

import org.smartboot.compare.ComparatorContext;
import org.smartboot.compare.comparator.AbstractComparator;
import org.smartboot.compare.constants.CompareConstants;
import org.smartboot.compare.difference.Difference;

/**
 * @author qinluo
 * @date 2024-03-24 01:40:51
 * @since 1.0.7
 */
public class BooleanPrimitiveArrayComparator extends AbstractComparator<boolean[]> {

    @Override
    public Difference compare(boolean[] expect, boolean[] actual, ComparatorContext<boolean[]> context) {
        return ArrayHelper.compare(expect, actual, context, CompareConstants.PRIMITIVE);
    }
}
