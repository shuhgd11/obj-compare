package org.smartboot.compare;

import java.util.Objects;

/**
 * @author qinluo
 * @version 1.0.0
 * @date 2020-08-18 16:56
 */
public class NameType {

    /**
     * Name
     */
    protected final String name;

    /**
     * Type
     */
    protected final Class<?> type;

    public NameType(String name, Class<?> type) {
        this.name = name;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public Class<?> getType() {
        return type;
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof NameType)) {
            return false;
        }

        NameType nt = (NameType)obj;
        return Objects.equals(nt.name, this.name) && nt.type == this.type;
    }

    @Override
    public String toString() {
        return "{" +
                "name:'" + name + '\'' +
                ", type=" + type +
                '}';
    }

    /**
     * Match name and any subclass of this type.
     */
    public boolean match(NameType nameType) {
        return Objects.equals(this.name, nameType.name)
                && this.type.isAssignableFrom(nameType.type);
    }

    public static NameType of(Class<?> type, String name) {
        return new NameType(name, type);
    }
}
