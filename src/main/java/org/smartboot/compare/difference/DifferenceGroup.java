package org.smartboot.compare.difference;

import org.smartboot.compare.comparator.Comparator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author qinluo
 * @date 2024-03-18 22:39:14
 * @since 1.0.7
 */
public class DifferenceGroup implements Difference {

    private final List<Difference> differences = new ArrayList<>();

    public List<Difference> getDifferences() {
        return differences;
    }

    public boolean hasDifferences() {
        return !differences.isEmpty();
    }

    public void addDifference(Difference difference) {
        if (difference == null) {
            return;
        }

        if (difference instanceof DifferenceGroup) {
            this.differences.addAll(((DifferenceGroup) difference).getDifferences());
        } else {
            this.differences.add(difference);
        }
    }

    public static DifferenceGroup of() {
        return new DifferenceGroup();
    }

    @Override
    public String getMessage() {
        throw new UnsupportedOperationException();
    }

    @Override
    public String type() {
        return "Group";
    }

    @Override
    public void attachSource(Comparator<?> source) {
        this.differences.forEach(p -> {
            if (p.getSource() == null) {
                p.attachSource(source);
            }
        });
    }
}
